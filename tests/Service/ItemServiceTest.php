<?php


namespace App\Tests\Service;


use App\Entity\Item;
use App\Entity\ToDoList;
use App\Entity\User;
use App\Repository\ItemRepository;
use App\Service\ItemService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ItemServiceTest extends KernelTestCase
{

    private $itemService;
    private $itemRepo;
    private $item;
    private $user;
    private $todo;

    protected function setUp()
    {

        $this->itemRepo = $this->createMock(ItemRepository::class);
        $this->itemService = new ItemService($this->itemRepo);

        $user = new User();
        $user->setFirstname('Mika')
            ->setLastname('John')
            ->setEmail("testUnit@gmail.com")
            ->setPassword('password8password')
            ->setAge((new \DateTime())->diff(new \DateTime('-31 years'), true)->y);

        $todo = new ToDoList();
        $todo->setUser($user)
            ->setName('To do list 1')
            ->setCreatedAt(new \DateTime());
        $user->setToDoList($todo);

        $item = new Item();
        $item->setToDoList($todo)
            ->setName('Item 1')
            ->setContent('Ceci est un test unit de Item class')
            ->setCreatedAt(new \DateTime('+1 days'));

        $todo->addItem($item);

        $this->item = $item;
        $this->todo = $todo;
        $this->user = $user;

    }


    public function testItemValid()
    {

        $this->assertEquals($this->item, $this->itemService->canAddItem($this->item));
    }

    public function testNameNullNoValid()
    {
        $this->item->setName('');
        $this->assertNull($this->itemService->canAddItem($this->item));
    }

    public function testContentNullNoValid()
    {
        $this->item->setContent('');
        $this->assertNull($this->itemService->canAddItem($this->item));
    }

    public function testContentLongNoValid()
    {

        $this->item->setContent(random_bytes(1200));

        $this->assertNull($this->itemService->canAddItem($this->item));
    }


    public function testToDoListNoValid()
    {

        $this->item->setToDoList(null);

        $this->assertNull($this->itemService->canAddItem($this->item));
    }

    public function testDateNoValid()
    {

        $this->item->setCreatedAt(null);

        $this->assertNull($this->itemService->canAddItem($this->item));
    }


    public function testNbItemsTodoListNoValid()
    {
        for ($i = 1; $i < 11; $i++) {
            $item = new Item();
            $item->setToDoList($this->todo)
                ->setName('Item' . $i)
                ->setContent('Ceci est un test unit de Item' . $i)
                ->setCreatedAt(new \DateTime());

            $this->todo->addItem($item);
        }

        $this->assertNull($this->itemService->canAddItem($this->item));
    }

    public function testMinitesDiffValid()
    {

        $datetime = new \DateTime('-2 days');
        $time = array(array('', $datetime->format("Y-m-d H:i:s")));
        $this->itemRepo->expects($this->any())
            ->method('findByLastDate')
            ->willReturn($time);

        $this->assertFalse($this->itemService->canAddItem($this->item) == $this->item);
    }

    public function testMinitesDiffNoValid()
    {

        $time = array(array('', (new \DateTime)->format("Y-m-d H:i:s")));
        $this->itemRepo->expects($this->any())
            ->method('findByLastDate')
            ->willReturn($time);

        $this->assertNull($this->itemService->canAddItem($this->item));
    }


}
